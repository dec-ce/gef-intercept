"use strict"

module.exports = function(gulp, plugins, options, errorHandler) {
  return function() {
    // Fonts found in bower_components/GEF/src/assets/fonts
    return gulp.src(options.paths.gef + "src/assets/fonts/**/*.{eot,svg,ttf,woff,woff2,otf}")
      .pipe(gulp.dest(options.paths.dist.assets.fonts))
      .on("error", errorHandler)
  }
}
