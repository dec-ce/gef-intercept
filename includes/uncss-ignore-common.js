module.exports = function() {

  var uncssIgnoreCommon = [
    /(\.gef-tabs.+)/,
    /(\.gef-tag-list.*)/,
    /(.+\.active.*)/,
    /(.+\.uk-active.*)/,
    /(.+\[aria-expanded.*)/,
    /(.+\[aria-hidden.*)/,
    /(.+iframe.*)/,
    /(.+placeholder.*)/,
    /(\.gef-ie9.+)/,
    /(\.gef-btt.+)/,
    /(\.gef-pseudo-link)/,
    /(\.gef-breadcrumbs.+)/,
    /(\.gef-pagination.+)/,
    /(\.call-out)/,
    /(\.gef-linkgroup-list.+)/,
    /(\.gef-tile-list.+)/,
    /(\.gef-search.+)/,
    /(\.gef-external-link)/,
    /\[ng:cloak\]/,
    /\[ng-cloak\]/,
    /\[data-ng-cloak\]/,
    /\[x-ng-cloak\]/,
    /(\.ng-cloak)/,
    /(\.x-ng-cloak)/
  ]

  return uncssIgnoreCommon;

}